export default {
    getRestaurants() {
        const res = fetch(`${process.env.VUE_APP_GOODFOOD_API}/restaurants?size=-1`,
        {
            method:'GET',
        }).then((response) => response.json());
        return res;
    }
}